package com.craApp.craApp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SOCIETE_SOUS_TRAITANT")
public class SocieteSousTraitant {
    
    @Id
    private Long idSociete;
    private String Designation;
    private String Contac;

    public Long getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(Long idSociete) {
        this.idSociete = idSociete;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getContac() {
        return Contac;
    }

    public void setContac(String contac) {
        Contac = contac;
    }

    public SocieteSousTraitant() {
    }

    public SocieteSousTraitant(Long idSociete, String designation, String contac) {
        this.idSociete = idSociete;
        Designation = designation;
        Contac = contac;
    }

    @Override
    public String toString() {
        return "SocieteSousTraitant [Contac=" + Contac + ", Designation=" + Designation + ", idSociete=" + idSociete
                + "]";
    }

    
}