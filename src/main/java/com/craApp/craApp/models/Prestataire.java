package com.craApp.craApp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PRESTATAIRE")
public class Prestataire {
    
    @Id
    private Long IdPrestataire;
    private String Nom;
    private String Prenom;
    private String Mail;
    private String Login;
    private String PassWord;
    private String Profil;
    private String ClePublique;
    private String ClePrivee;
    private Long idConnexionManager;
    private Long idSociete;
    private double Solde;

    public Long getIdPrestataire() {
        return IdPrestataire;
    }

    public void setIdPrestataire(Long idPrestataire) {
        IdPrestataire = idPrestataire;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setPassWord(String passWord) {
        PassWord = passWord;
    }

    public String getProfil() {
        return Profil;
    }

    public void setProfil(String profil) {
        Profil = profil;
    }

    public String getClePublique() {
        return ClePublique;
    }

    public void setClePublique(String clePublique) {
        ClePublique = clePublique;
    }

    public String getClePrivee() {
        return ClePrivee;
    }

    public void setClePrivee(String clePrivee) {
        ClePrivee = clePrivee;
    }

    public Long getIdConnexionManager() {
        return idConnexionManager;
    }

    public void setIdConnexionManager(Long idConnexionManager) {
        this.idConnexionManager = idConnexionManager;
    }

    public Long getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(Long idSociete) {
        this.idSociete = idSociete;
    }

    public double getSolde() {
        return Solde;
    }

    public void setSolde(double solde) {
        Solde = solde;
    }

    public Prestataire() {
    }

    public Prestataire(Long idPrestataire, String nom, String prenom, String mail, String login, String passWord,
            String profil, String clePublique, String clePrivee, Long idConnexionManager, Long idSociete,
            double solde) {
        IdPrestataire = idPrestataire;
        Nom = nom;
        Prenom = prenom;
        Mail = mail;
        Login = login;
        PassWord = passWord;
        Profil = profil;
        ClePublique = clePublique;
        ClePrivee = clePrivee;
        this.idConnexionManager = idConnexionManager;
        this.idSociete = idSociete;
        Solde = solde;
    }

    @Override
    public String toString() {
        return "Prestataire [ClePrivee=" + ClePrivee + ", ClePublique=" + ClePublique + ", IdPrestataire="
                + IdPrestataire + ", Login=" + Login + ", Mail=" + Mail + ", Nom=" + Nom + ", PassWord=" + PassWord
                + ", Prenom=" + Prenom + ", Profil=" + Profil + ", Solde=" + Solde + ", idConnexionManager="
                + idConnexionManager + ", idSociete=" + idSociete + "]";
    }

    
    
}