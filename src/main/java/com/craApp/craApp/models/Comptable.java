package com.craApp.craApp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COMPTABLE")
public class Comptable {

    @Id
    private Long IdConnexion;
    private String Nom;
    private String Prenom;
    private String Mail;
    private String Login;
    private String PassWord;
    private String Profil;

    public Long getIdConnexion() {
        return IdConnexion;
    }

    public void setIdConnexion(Long idConnexion) {
        IdConnexion = idConnexion;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setPassWord(String passWord) {
        PassWord = passWord;
    }

    public String getProfil() {
        return Profil;
    }

    public void setProfil(String profil) {
        Profil = profil;
    }

    public Comptable(Long idConnexion, String nom, String prenom, String mail, String login, String passWord,
            String profil) {
        IdConnexion = idConnexion;
        Nom = nom;
        Prenom = prenom;
        Mail = mail;
        Login = login;
        PassWord = passWord;
        Profil = profil;
    }

    public Comptable() {
    }

    @Override
    public String toString() {
        return "Comptable [IdConnexion=" + IdConnexion + ", Login=" + Login + ", Mail=" + Mail + ", Nom=" + Nom
                + ", PassWord=" + PassWord + ", Prenom=" + Prenom + ", Profil=" + Profil + "]";
    }
    
    
}