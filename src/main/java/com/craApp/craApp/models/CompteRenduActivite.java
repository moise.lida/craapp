package com.craApp.craApp.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COMPTE_RENDU_dACTIVITE")
public class CompteRenduActivite {
    
    @Id
    private Long idCRA;
    private Long idConnexionManager;
    private Long idConnexionPrestataire;
    private String HashCRA;
    private String idTransactionManager;
    private String idTransactionPrestataire;
    private Date DateHeureSignatureManager;
    private Date DateHeureSignaturePrestataire;

    public Long getIdCRA() {
        return idCRA;
    }

    public void setIdCRA(Long idCRA) {
        this.idCRA = idCRA;
    }

    public Long getIdConnexionManager() {
        return idConnexionManager;
    }

    public void setIdConnexionManager(Long idConnexionManager) {
        this.idConnexionManager = idConnexionManager;
    }

    public Long getIdConnexionPrestataire() {
        return idConnexionPrestataire;
    }

    public void setIdConnexionPrestataire(Long idConnexionPrestataire) {
        this.idConnexionPrestataire = idConnexionPrestataire;
    }

    public String getHashCRA() {
        return HashCRA;
    }

    public void setHashCRA(String hashCRA) {
        HashCRA = hashCRA;
    }

    public String getIdTransactionManager() {
        return idTransactionManager;
    }

    public void setIdTransactionManager(String idTransactionManager) {
        this.idTransactionManager = idTransactionManager;
    }

    public String getIdTransactionPrestataire() {
        return idTransactionPrestataire;
    }

    public void setIdTransactionPrestataire(String idTransactionPrestataire) {
        this.idTransactionPrestataire = idTransactionPrestataire;
    }

    public Date getDateHeureSignatureManager() {
        return DateHeureSignatureManager;
    }

    public void setDateHeureSignatureManager(Date dateHeureSignatureManager) {
        DateHeureSignatureManager = dateHeureSignatureManager;
    }

    public Date getDateHeureSignaturePrestataire() {
        return DateHeureSignaturePrestataire;
    }

    public void setDateHeureSignaturePrestataire(Date dateHeureSignaturePrestataire) {
        DateHeureSignaturePrestataire = dateHeureSignaturePrestataire;
    }

    public CompteRenduActivite() {
    }

    public CompteRenduActivite(Long idCRA, Long idConnexionManager, Long idConnexionPrestataire, String hashCRA,
            String idTransactionManager, String idTransactionPrestataire, Date dateHeureSignatureManager,
            Date dateHeureSignaturePrestataire) {
        this.idCRA = idCRA;
        this.idConnexionManager = idConnexionManager;
        this.idConnexionPrestataire = idConnexionPrestataire;
        HashCRA = hashCRA;
        this.idTransactionManager = idTransactionManager;
        this.idTransactionPrestataire = idTransactionPrestataire;
        DateHeureSignatureManager = dateHeureSignatureManager;
        DateHeureSignaturePrestataire = dateHeureSignaturePrestataire;
    }

    @Override
    public String toString() {
        return "CompteRenduActivite [DateHeureSignatureManager=" + DateHeureSignatureManager
                + ", DateHeureSignaturePrestataire=" + DateHeureSignaturePrestataire + ", HashCRA=" + HashCRA
                + ", idCRA=" + idCRA + ", idConnexionManager=" + idConnexionManager + ", idConnexionPrestataire="
                + idConnexionPrestataire + ", idTransactionManager=" + idTransactionManager
                + ", idTransactionPrestataire=" + idTransactionPrestataire + "]";
    }


    
}