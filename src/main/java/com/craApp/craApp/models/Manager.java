package com.craApp.craApp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MANAGER")
public class Manager {

    @Id
    private Long idConnexion;
    private String Nom;
    private String Prenom;
    private String Mail;
    private String Login;
    private String PassWord;
    private String Profil;
    private String ClePublique;
    private String ClePrivee;
    private double Solde;

    public Long getIdConnexion() {
        return idConnexion;
    }

    public void setIdConnexion(Long idConnexion) {
        this.idConnexion = idConnexion;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setPassWord(String passWord) {
        PassWord = passWord;
    }

    public String getProfil() {
        return Profil;
    }

    public void setProfil(String profil) {
        Profil = profil;
    }

    public String getClePublique() {
        return ClePublique;
    }

    public void setClePublique(String clePublique) {
        ClePublique = clePublique;
    }

    public String getClePrivee() {
        return ClePrivee;
    }

    public void setClePrivee(String clePrivee) {
        ClePrivee = clePrivee;
    }

    public double getSolde() {
        return Solde;
    }

    public void setSolde(double solde) {
        Solde = solde;
    }

    public Manager() {
    }

    public Manager(Long idConnexion, String nom, String prenom, String mail, String login, String passWord,
            String profil, String clePublique, String clePrivee, double solde) {
        this.idConnexion = idConnexion;
        Nom = nom;
        Prenom = prenom;
        Mail = mail;
        Login = login;
        PassWord = passWord;
        Profil = profil;
        ClePublique = clePublique;
        ClePrivee = clePrivee;
        Solde = solde;
    }

    @Override
    public String toString() {
        return "Manager [ClePrivee=" + ClePrivee + ", ClePublique=" + ClePublique + ", Login=" + Login + ", Mail="
                + Mail + ", Nom=" + Nom + ", PassWord=" + PassWord + ", Prenom=" + Prenom + ", Profil=" + Profil
                + ", Solde=" + Solde + ", idConnexion=" + idConnexion + "]";
    }


    
}