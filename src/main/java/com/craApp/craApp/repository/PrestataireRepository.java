package com.craApp.craApp.repository;

import com.craApp.craApp.models.Prestataire;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrestataireRepository extends JpaRepository<Prestataire, Long> {

    Prestataire findAndDeletePrestataire(Long IdConnexion);
    
}