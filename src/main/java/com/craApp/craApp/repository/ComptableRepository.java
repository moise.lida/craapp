package com.craApp.craApp.repository;

import com.craApp.craApp.models.Comptable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComptableRepository extends JpaRepository<Comptable, Long>{

    Comptable findMailAndPassword(String mail, String password);
    Comptable findAndDeleteComptable(Long IdComptable);
    Comptable findAndUpdateComptable(Long IdComptable);
    
}