package com.craApp.craApp.repository;

import com.craApp.craApp.models.Manager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long>{
    Manager findAndDeleteManager(Long IdConnexion);
    Manager findAndUpdateManager(Long IdConnexion);
}