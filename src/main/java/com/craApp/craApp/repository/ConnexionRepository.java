package com.craApp.craApp.repository;

import com.craApp.craApp.models.Connexion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnexionRepository extends JpaRepository<Connexion, Long> {

}