package com.craApp.craApp.repository;

import com.craApp.craApp.models.CompteRenduActivite;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompterenduactiviteRepository extends JpaRepository<CompteRenduActivite, Long>{
    
}