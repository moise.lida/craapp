package com.craApp.craApp.repository;

import com.craApp.craApp.models.SocieteSousTraitant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SocietesoustraitantRepository extends JpaRepository<SocieteSousTraitant, Long>{
    
}