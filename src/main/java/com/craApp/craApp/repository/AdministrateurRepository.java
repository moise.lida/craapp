package com.craApp.craApp.repository;

import com.craApp.craApp.models.Administrateur;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministrateurRepository extends JpaRepository<Administrateur, Long> {
    
    Administrateur findMailAndPassword(String mail, String password);
    Administrateur findAndDeleteAdministrateur(Long IdConnexion);
    Administrateur findAndUpdateAdministrateur(Long IdConnexion);
}