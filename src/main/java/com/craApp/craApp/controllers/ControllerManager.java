package com.craApp.craApp.controllers;

import javax.persistence.PostUpdate;

import com.craApp.craApp.models.Manager;
import com.craApp.craApp.repository.ManagerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/v1/manager")
public class ControllerManager {

    @Autowired
    private ManagerRepository managerRepository;

    @GetMapping(value = "/")
    public ResponseEntity finfAll(){
        return ResponseEntity.ok(managerRepository.findAll());
    }

    @GetMapping(value = "/{IdConnexion}")
    public ResponseEntity findManagerById(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot find manager with null IdConnexion");
        }

        Manager manager = managerRepository.getOne(IdConnexion);
        if(IdConnexion == null ){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(manager);
    }

    @PostMapping(value = "/")
    public ResponseEntity createManager(@RequestBody Manager  manager){
        if(manager == null){
            return ResponseEntity.badRequest().body("Cannot create manager with empty fields");
        }

        Manager createManager = managerRepository.save(manager);
        return ResponseEntity.ok().body(createManager);
    }
    
     @DeleteMapping(value = "/")
    public ResponseEntity deleteManager(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot delete manager with null IdConnexion");
        }
       Manager deleteManager = managerRepository.findAndDeleteManager(IdConnexion);
       if(IdConnexion == null){
           return ResponseEntity.notFound().build();
       }

       return ResponseEntity.ok(deleteManager);
    }

    @PostUpdate
    public ResponseEntity updateManager(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Canoot update manager with null IdConnexion");
        }

        Manager udateManager = managerRepository.findAndUpdateManager(IdConnexion);
        if(IdConnexion == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(udateManager);
    }
}
