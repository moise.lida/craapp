package com.craApp.craApp.controllers;

import com.craApp.craApp.models.Prestataire;
import com.craApp.craApp.repository.PrestataireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/prestataire")
public class controllerPrestataire {
    
    @Autowired
    private PrestataireRepository prestataireRepository;

    @GetMapping(value = "/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(prestataireRepository.findAll());
    }

    @RequestMapping(value = "/{IdConnexion}")
    public ResponseEntity findPrestataireById(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot find prestataire with null id");
        }

        Prestataire prestataire = prestataireRepository.getOne(IdConnexion);
        if(IdConnexion == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(prestataire);
    }

    @PostMapping(value = "/")
    public ResponseEntity createPrestataire(@RequestBody Prestataire prestataire){
        if(prestataire == null){
            return ResponseEntity.badRequest().body("Cannot create prestataire with null Id");
        }
        Prestataire createPrestataire = prestataireRepository.save(prestataire);
        return ResponseEntity.ok().body(createPrestataire);
    }

    @DeleteMapping("/{IdConnexion}")
    public ResponseEntity deletePrestataire(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot delete prestataire with null Id");
        }
        Prestataire deletePrestataire = prestataireRepository.findAndDeletePrestataire(IdConnexion);
        if(IdConnexion == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(deletePrestataire);
    }
}
