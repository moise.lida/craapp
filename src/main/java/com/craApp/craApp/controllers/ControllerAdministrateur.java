package com.craApp.craApp.controllers;


import javax.persistence.PostUpdate;

import com.craApp.craApp.models.Administrateur;
import com.craApp.craApp.repository.AdministrateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/v1/Administrateur")
public class ControllerAdministrateur {
    
    @Autowired
    private AdministrateurRepository administrateurRepository;

    //Affichage de tous les utilisateurs
    @GetMapping(value = "/")
    public ResponseEntity findAll(){

        return ResponseEntity.ok(administrateurRepository.findAll());
    }

    //Methode de recherche
    @GetMapping("/{IdConnexion}")
   public ResponseEntity findAdministrateurById(@PathVariable(name = "IdConnexion") Long IdConnexion){
       if(IdConnexion == null){
           return ResponseEntity.badRequest().body("Cannot found administrator with null IdConnexion");
       }

       Administrateur administrateur = administrateurRepository.getOne(IdConnexion);
       if(IdConnexion == null){
           return ResponseEntity.notFound().build();
       }

       return ResponseEntity.ok().body(administrateur);

   }

   //Methode de creation
   @PostMapping(value = "/")
   public ResponseEntity createAdminitrator(@RequestBody Administrateur administrateur){

    if(administrateur == null){
        return ResponseEntity.badRequest().body("Cannot create administrator with empty fields");
    }

    Administrateur createAdministrateur = administrateurRepository.save(administrateur);
    return ResponseEntity.ok().body(createAdministrateur);

   }

    @PostUpdate
   public ResponseEntity updateAdministrator(@PathVariable(name="IdConnexion") Long IdConnexion){
    if(IdConnexion == null){
        return ResponseEntity.badRequest().body("Cannot update administrator with null id");
    }

    Administrateur updateAdministrator = administrateurRepository.findAndUpdateAdministrateur(IdConnexion);
    if(IdConnexion == null){
        return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(updateAdministrator);
   } 

   //Méthode de connexion
   @PostMapping(value = "/login")
   public ResponseEntity loginAdministrator(@RequestParam(name = "mail") String mail, @RequestParam(name = "password") String password){
      if(org.springframework.util.StringUtils.isEmpty(mail) || org.springframework.util.StringUtils.isEmpty(password)){
        return ResponseEntity.badRequest().body("Cannot login with empty mail and password");
      }

      Administrateur authenticaAdministrateur = administrateurRepository.findMailAndPassword(mail, password);
      if(authenticaAdministrateur == null){
          return ResponseEntity.notFound().build();
      }

      return ResponseEntity.ok(authenticaAdministrateur);
   } 

   //Methode de suppression
    @DeleteMapping(value = "/{IdConnexion}")
   public ResponseEntity deleteAdministrator(@PathVariable(name = "IdConnexion") Long IdConnexion){
    if(IdConnexion == null){
        return ResponseEntity.badRequest().body("Cannot delete administrator with null IdConnexion");
    }

    Administrateur deleteAdministrateur = administrateurRepository.findAndDeleteAdministrateur(IdConnexion);
    if(deleteAdministrateur == null){
        return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(deleteAdministrateur);
   } 
}

