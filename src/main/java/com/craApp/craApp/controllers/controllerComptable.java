package com.craApp.craApp.controllers;

import javax.persistence.PostUpdate;

import com.craApp.craApp.models.Comptable;
import com.craApp.craApp.repository.ComptableRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/comptable")
public class controllerComptable {

    @Autowired
    private ComptableRepository comptableRepository;

    @RequestMapping(value = "/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(comptableRepository.findAll());
    }
    
    @RequestMapping(value = "/{IdConnexion}")
    public ResponseEntity findComptableById(@PathVariable(name = "IdConnexion") Long IdConnexion){

        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot found comptable with null IdConnexion");
        }
        Comptable comptable = comptableRepository.getOne(IdConnexion);
        if(IdConnexion == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(comptable);
    }

    @PostMapping(value = "/")
    public ResponseEntity createComptable(@RequestBody Comptable comptable){

        if(comptable == null){
            return ResponseEntity.badRequest().body("Cannot found comptable with empty field");
        }

        Comptable createComptable = comptableRepository.save(comptable);
        return ResponseEntity.ok().body(createComptable);
    }

    @PostUpdate
    public ResponseEntity updateComptable(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if (IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot update comptable with null id");
        }
        
        Comptable updateComptable = comptableRepository.findAndUpdateComptable(IdConnexion);
        if(IdConnexion == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(updateComptable);
    }

    @DeleteMapping(value = "/{IdConnexion}")
    public ResponseEntity deleteComptable(@PathVariable(name = "IdConnexion") Long IdConnexion){
        if(IdConnexion == null){
            return ResponseEntity.badRequest().body("Cannot delete comptable with null idConnexion");
        }

        Comptable deleteComptable = comptableRepository.findAndDeleteComptable(IdConnexion);
        if(deleteComptable == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(deleteComptable);
    } 

    /* @PostMapping(value = "/login")
    public ResponseEntity loginComptable(@RequestParam(name = "mail") String mail, @RequestParam(name = "password") String password){

        if(utils)
    } */
}
